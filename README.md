Список багов, найденных на сайтах, которыми я пользуюсь (RZD, Telegram, TJournal)

1. [RZD](https://gitlab.com/iveina2021/cv-demo-issues/-/issues/1) - Некорректное отображение главной страницы https://www.rzd.ru при низкой скорости загрузки
2. [Telegram](https://gitlab.com/iveina2021/cv-demo-issues/-/issues/2) - Невозможно выбрать аудиоустройство (наушники/громкая связь) в режиме видеозвонка на iPhone
3. [TJournal](https://gitlab.com/iveina2021/cv-demo-issues/-/issues/3) - При наведении на иконки внизу поста 2 из 3 title не отображаются

![RZD](img/RZD%20for%20CV.png)
![TG](img/TG%20for%20CV.png)
![TJ](img/TJ%20for%20CV.jpg)


